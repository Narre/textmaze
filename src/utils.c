//  TextMaze
//  Copyleft 🄯 2022 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "utils.h"

#include <string.h>
#include "version.h"

char *concat_strings(const char *restrict c1, const char *restrict c2) {
	char *ret;
	ret = malloc((strlen(c1) + strlen(c2) + 1) * sizeof(char));
	for(register unsigned int i = 0; i < strlen(c1); ++i) ret[i] = c1[i];
	for(register unsigned int i = 0; i < strlen(c2); ++i) ret[strlen(c1) + i] = c2[i];
	ret[strlen(c1) + strlen(c2)] = '\0';
	return ret;
}

void save_game(const int slot, const int level, const int score) {
	FILE *f;
	switch(slot) {
		case 1: f = fopen("../savegame/slot1.msg", "w"); break;
		case 2: f = fopen("../savegame/slot2.msg", "w"); break;
		case 3: f = fopen("../savegame/slot3.msg", "w"); break;
		case 4: f = fopen("../savegame/slot4.msg", "w"); break;
		case 5: f = fopen("../savegame/slot5.msg", "w"); break;
		default: return;
	}
	fprintf(f, "maze-savegame\nversion %i\nlevel %i\nscore %i\n", FORMAT_VERSION, level, score);
	fclose(f);
	return;
}
