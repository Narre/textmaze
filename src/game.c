//  TextMaze
//  Copyleft 🄯 2022 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

#include <string.h>
#include "menu.h"
#include "screen_manager.h"
#include "utils.h"
#include "version.h"

unsigned char load_savegame(const char *restrict filename) {
	FILE *f = fopen(filename, "r");
	if(!f) return 1;
	char *str;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "maze-savegame")) {
		free(str);
		fclose(f);
		return 1;
	}
	free(str);
	fscanf(f, "%ms", &str);
	if(strcmp(str, "version")) {
		free(str);
		fclose(f);
		return 1;
	}
	free(str);
	int num;
	fscanf(f, "%i", &num);
	if(num != FORMAT_VERSION) {
		fclose(f);
		return 1;
	}
	fscanf(f, "%ms", &str);
	if(strcmp(str, "level")) {
		free(str);
		fclose(f);
		return 1;
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		return 1;
	}
	int lvl = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "score")) {
		free(str);
		return 1;
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 0) {
		fclose(f);
		return 1;
	}
	fclose(f);
	start_game((unsigned int)lvl, (unsigned int)num);
	return 0;
}

void new_game(void) {
	start_game(1, 0);
}

static void crash(void) {
	clear_screen();
	system("stty -raw echo");
	fprintf(stderr, "Error! Couldn't parse file!\n");
	exit(200);
}

void start_game(unsigned int level, unsigned int score) {
	char end[] = ".ml";
	char num_[10];
	sprintf(num_, "%i", level);
	char *c1 = concat_strings("../levels/level", num_);
	char *c2 = concat_strings(c1, end);
	free(c1);
	FILE *f = fopen(c2, "r");
	free(c2);
	if(!f) {
		clear_screen();
		finish_game(score);
		return;
	}
	int width, height, start_x, start_y, finish_x, finish_y, coins, required, *positions_x, *positions_y;
	char **data;
	char *str;
	int num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "maze-level")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%ms", &str);
	if(strcmp(str, "version")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num != FORMAT_VERSION) {
		fclose(f);
		crash();
	}
	fscanf(f, "%ms", &str);
	if(strcmp(str, "size")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	width = num;
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	height = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "start")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	start_x = num;
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	start_y = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "finish")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	finish_x = num;
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	finish_y = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "coins")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	coins = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "required")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	fscanf(f, "%i", &num);
	if(num < 1) {
		fclose(f);
		crash();
	}
	required = num;
	fscanf(f, "%ms", &str);
	if(strcmp(str, "positions")) {
		free(str);
		fclose(f);
		crash();
	}
	free(str);
	int px = start_x, py = start_y;
	positions_x = (int *)malloc(coins * sizeof(int));
	positions_y = (int *)malloc(coins * sizeof(int));
	for(register int i = 0; i < coins; ++i) {
		fscanf(f, "%i %i", &positions_x[i], &positions_y[i]);
		if(positions_x[i] < 0 || positions_y[i] < 0 || positions_x[i] >= width || positions_y[i] >= height) {
			free(str);
			free(positions_x);
			free(positions_y);
			fclose(f);
			crash();
		}
	}
	fscanf(f, "%ms", &str);
	if(strcmp(str, "data")) {
		free(str);
		free(positions_x);
		free(positions_y);
		fclose(f);
		crash();
	}
	free(str);
	data = (char **)malloc(width * sizeof(char *));
	for(register int i = 0; i < width; ++i) data[i] = (char *)malloc(height * sizeof(char));
	getc(f);
	for(register int i = 0; i < height; ++i) {
		for(register int j = 0; j < width; ++j) {
			fscanf(f, "%c", &data[j][i]);
			if(data[j][i] != '@' && data[j][i] != '.') {
				for(register int k = 0; k < width; ++k) free(data[k]);
				free(data);
				free(positions_x);
				free(positions_y);
				fclose(f);
				crash();
			}
		}
		char c;
		c = getc(f);
		if(c != '\n') {
			for(register int k = 0; k < width; ++k) free(data[k]);
			free(data);
			free(positions_x);
			free(positions_y);
			fclose(f);
			crash();
		}
	}
	fclose(f);
	data[start_x][start_y] = '*';
	if(required) data[finish_x][finish_y] = '!';
	else data[finish_x][finish_y] = '&';
	for(register int i = 0; i < coins; ++i) {
		data[positions_x[i]][positions_y[i]] = 'X';
	}
	unsigned char exit = 0, next_level = 0, skip_draw = 0;
	while(!next_level && !exit) {
		if(!skip_draw) {
			clear_screen();
			draw_level(level, (const char **)data, width, height, score, required);
		} else skip_draw = 0;
		int init_score = score;
		char c = getc(stdin);
		switch(c) {
			case 'w':
			case 'W':
			case '8':
			case 'i':
			case 'I':
				if(data[px][py - 1] == '@' || data[px][py - 1] == '!') {
					skip_draw = 1;
					continue;
				}
				if(data[px][py - 1] == 'X') {
					score += 100;
					if(required) {
						--required;
						if(!required) data[finish_x][finish_y] = '&';
					}
					data[px][py] = '.';
					data[px][py - 1] = '*';
					--py;
					continue;
				}
				if(data[px][py - 1] == '.') {
					data[px][py] = '.';
					data[px][py - 1] = '*';
					--py;
					continue;
				}
				if(data[px][py - 1] == '&') {
					score += 1000;
					next_level = 1;
					break;
				}
				break;
			case 'a':
			case 'A':
			case '4':
			case 'j':
			case 'J':
				if(data[px - 1][py] == '@' || data[px - 1][py] == '!') {
					skip_draw = 1;
					continue;
				}
				if(data[px - 1][py] == 'X') {
					score += 100;
					if(required) {
						--required;
						if(!required) data[finish_x][finish_y] = '&';
					}
					data[px][py] = '.';
					data[px - 1][py] = '*';
					--px;
					continue;
				}
				if(data[px - 1][py] == '.') {
					data[px][py] = '.';
					data[px - 1][py] = '*';
					--px;
					continue;
				}
				if(data[px - 1][py] == '&') {
					score += 1000;
					next_level = 1;
					break;
				}
				break;
			case 's':
			case 'S':
			case '5':
			case '2':
			case 'k':
			case 'K':
				if(data[px][py + 1] == '@' || data[px][py + 1] == '!') {
					skip_draw = 1;
					continue;
				}
				if(data[px][py + 1] == 'X') {
					score += 100;
					if(required) {
						--required;
						if(!required) data[finish_x][finish_y] = '&';
					}
					data[px][py] = '.';
					data[px][py + 1] = '*';
					++py;
					continue;
				}
				if(data[px][py + 1] == '.') {
					data[px][py] = '.';
					data[px][py + 1] = '*';
					++py;
					continue;
				}
				if(data[px][py + 1] == '&') {
					score += 1000;
					next_level = 1;
					break;
				}
				break;
			case 'd':
			case 'D':
			case '6':
			case 'l':
			case 'L':
				if(data[px + 1][py] == '@' || data[px + 1][py] == '!') {
					skip_draw = 1;
					continue;
				}
				if(data[px + 1][py] == 'X') {
					score += 100;
					if(required) {
						--required;
						if(!required) data[finish_x][finish_y] = '&';
					}
					data[px][py] = '.';
					data[px + 1][py] = '*';
					++px;
					continue;
				}
				if(data[px + 1][py] == '.') {
					data[px][py] = '.';
					data[px + 1][py] = '*';
					++px;
					continue;
				}
				if(data[px + 1][py] == '&') {
					score += 1000;
					next_level = 1;
					break;
				}
				break;
			case 'p':
			case 'P':
				clear_screen();
				if(pause_menu(level, init_score)) {
					exit = 1;
					break;
				}			
			default: continue;
		}
	}
	for(register int i = 0; i < width; ++i) free(data[i]);
	free(data);
	free(positions_x);
	free(positions_y);
	if(exit) return;
	start_game(level + 1, score);
}
