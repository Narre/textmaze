//  TextMaze
//  Copyleft 🄯 2022 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "menu.h"

#include "game.h"
#include "screen_manager.h"
#include "utils.h"
#include "version.h"

void draw_main_menu(void) {
	printf("     M   M   A   ZZZZZ EEEEE\n\r");
	printf("     MM MM  A A     Z  E\n\r");
	printf("     M M M A   A   Z   EEEE\n\r");
	printf("     M   M AAAAA  Z    E\n\r");
	printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
	printf("            MAIN MENU\n\r");
	printf("          1.   New Game\n\r");
	printf("          2.  Load Game\n\r");
	printf("          3.       Help\n\r");
	printf("          4.    Licence\n\n\r");
	printf("          0.       Quit\n\n\n\r");
	printf("TextMaze v%s\n\rCopyleft 🄯 2022 Jiří Paleček\n\r", VERSION);
	printf("This program comes with ABSOLUTELY NO WARRANTY.\n\rThis is free software, and you are welcome to redistribute it under certain\n\rconditions. Print the licence from the main menu for more details.");
}

void load_game_menu(void) {
	printf("     M   M   A   ZZZZZ EEEEE\n\r");
	printf("     MM MM  A A     Z  E\n\r");
	printf("     M M M A   A   Z   EEEE\n\r");
	printf("     M   M AAAAA  Z    E\n\r");
	printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
	printf("            LOAD GAME\n\r");
	printf("          1.     Slot 1\n\r");
	printf("          2.     Slot 2\n\r");
	printf("          3.     Slot 3\n\r");
	printf("          4.     Slot 4\n\r");
	printf("          5.     Slot 5\n\n\r");
	printf("          0.       Back");
	while(1) {
		char c = getc(stdin);
		char retval;
		switch(c) {
			case '0': return;
			case '1': retval = load_savegame("../savegame/slot1.msg"); break;
			case '2': retval = load_savegame("../savegame/slot2.msg"); break;
			case '3': retval = load_savegame("../savegame/slot3.msg"); break;
			case '4': retval = load_savegame("../savegame/slot4.msg"); break;
			case '5': retval = load_savegame("../savegame/slot5.msg"); break;
			default:  continue;
		}
		if(retval) {
			clear_screen();
			system("stty -raw echo");
			fprintf(stderr, "Error! Failed to open savegame file or the file is not a valid maze-savegame!\n");
			exit(100);
		}
		else return;
	}
}

void print_help(void) {
	printf("     M   M   A   ZZZZZ EEEEE\n\r");
	printf("     MM MM  A A     Z  E\n\r");
	printf("     M M M A   A   Z   EEEE\n\r");
	printf("     M   M AAAAA  Z    E\n\r");
	printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
	printf("              HELP:\n\n\r");
	printf("Use numbers to navigate menus (0 is usually back)\n\r");
	printf("You are the *\n\r");
	printf("@ is a wall\n\r");
	printf("X is a coin that you can collect.\n\r");
	printf("! is the locked end of the level. You need more coins to unlock it!\n\r");
	printf("& is the unlocked end of the level.\n\r");
	printf("Use the 'P' key to bring up the pause menu.\n\r");
	printf("Use WASD, IJKL or the numpad to navigate.\n\n\r");
	printf("           0.      Back");
	while(1) {
		char c = getc(stdin);
		if(c == '0') return;
	}
}

void finish_game(int score) {
	printf("     M   M   A   ZZZZZ EEEEE\n\r");
	printf("     MM MM  A A     Z  E\n\r");
	printf("     M M M A   A   Z   EEEE\n\r");
	printf("     M   M AAAAA  Z    E\n\r");
	printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
	printf("         CONGRATULATIONS!\n\r");
	printf("      You finished the game!\n\n\r");
	printf("      SCORE: %i\n\n\r", score);
	printf("          0.  Main Menu");
	while(1) {
		char c = getc(stdin);
		if(c == '0') return;
	}
}

unsigned char pause_menu(int level, int score) {
	while(1) {
		clear_screen();
		printf("     M   M   A   ZZZZZ EEEEE\n\r");
		printf("     MM MM  A A     Z  E\n\r");
		printf("     M M M A   A   Z   EEEE\n\r");
		printf("     M   M AAAAA  Z    E\n\r");
		printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
		printf("           GAME PAUSED\n\r");
		printf("          1.  Save game\n\r");
		printf("          2.  Exit game\n\n\r");
		printf("          0.   Continue");
		char c = getc(stdin);
		switch(c) {
			case '0': return 0;
			case '1': clear_screen(); save_menu(level, score); continue;
			case '2': return 1;
			default:  continue;
		}
	}
}

void save_menu(int level, int score) {
	printf("     M   M   A   ZZZZZ EEEEE\n\r");
	printf("     MM MM  A A     Z  E\n\r");
	printf("     M M M A   A   Z   EEEE\n\r");
	printf("     M   M AAAAA  Z    E\n\r");
	printf("     M   M A   A ZZZZZ EEEEE  v%s\n\n\r", VERSION);
	printf("            SAVE GAME\n\r");
	printf("          1.     Slot 1\n\r");
	printf("          2.     Slot 2\n\r");
	printf("          3.     Slot 3\n\r");
	printf("          4.     Slot 4\n\r");
	printf("          5.     Slot 5\n\n\r");
	printf("          0.     Cancel");
	unsigned char done = 0;
	while(!done) {
		char c = getc(stdin);
		switch(c) {
			case '0': return;
			case '1': done = 1; save_game(1, level, score); continue;
			case '2': done = 1; save_game(2, level, score); continue;
			case '3': done = 1; save_game(3, level, score); continue;
			case '4': done = 1; save_game(4, level, score); continue;
			case '5': done = 1; save_game(5, level, score); continue;
			default:  continue;
		}
	}
}

void print_licence(void) {
	system("stty -raw echo");
	system("cat ../LICENCE");
	system("stty raw -echo");
	printf("\n\n\rPress any key to return to the main menu...");
	getc(stdin);
}
