//  TextMaze
//  Copyleft 🄯 2022 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>
#include "screen_manager.h"
#include "menu.h"
#include "game.h"

int main(void) {
	init_screen();
	while(1) {
		clear_screen();
		draw_main_menu();
		char c = getc(stdin);
		switch(c) {
			case '1': new_game(); break;
			case '2': clear_screen(); load_game_menu(); break;
			case '3': clear_screen(); print_help(); break;
			case '4': clear_screen(); print_licence(); break;
			case '0': clear_screen(); system("stty -raw echo"); printf("Hope you enjoyed!\n"); return 0;
			default:  continue;
		}
	}
}
