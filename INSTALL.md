**TextMaze**
*aka "MAZE"*
v0.0.0

This game is specifically made for Linux and relies on a linux shell! (bash, probably works with others)\
Do not try to compile this game on other systems, it's not gonna work (even if it compiles)!

To compile the game, use the provided Makefile in the *build/* directory. That will create an\
executable binary file (*maze*) in the same directory. Please, do not move this binary elsewhere,\
the game is not *yet* clever enough to be able to find its assets if it's somewhere else, and that\
includes the game's root directory.

There is currently no way to install the game system-wide in this version.
