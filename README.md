**TextMaze**
*aka "MAZE"*
v0.0.0

This game was originally created by Narre on 3 January 2022

TextMaze is a simple text-based maze game for Linux with easily definable levels\
in an intuitive text format. It has a savegame feature allowing users to save their\
progress if there are a lot of levels.

The player is awarded 100 poins for collecting a coin and 1000 points for finishing\
a level. Each level can require a minimum amount of coins for the exit to unlock.

The game is controlled using the W, A, S and D keys.

The point of this program isn't to provide a set of mazes for the user to solve,\
but mainly to allow users to easily share their own mazes and play them using this\
software. That's why there are only a handful of example levels bundled with it.

Currently, the game can only load mazes from levels/level{number}.ml\
Future versions will allow for multiple sets of mazes to coexist and\
be loaded independently from the main menu (please remember, this initial\
version was only thrown together in 4 hours!)

This software and all its parts are licenced under the GNU General Public Licence,\
version 3. See the LICENCE file for more details.
