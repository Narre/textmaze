**TextMaze**
*aka "MAZE"*
v0.0.0

If you wish to contribute code, please try to keep using the same coding style the game already\
uses consistently. If you want to submit your code, create a merge request in the GitLab repo.

If you wish to report bugs or ideas, please feel free to open an issue on GitLab. 
